import createPersistedState from 'vuex-persistedstate'
import storeVuex from "@/store/store";

const persistVuexStates = Object.assign({}, {
  login: storeVuex.storeVuexState.login
});

export default ({store}) => {
  createPersistedState({

    key: 'vuex',

    paths: persistVuexStates

  })(store)
}




