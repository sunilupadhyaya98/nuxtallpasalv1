import Api from "@/services/Api";
// import store from "@/store";

function login(payload) {

  const url = '/store-login';

  const headers = {
    'Accept': 'application/json',
    'X-Requested-with': 'XMLHttpRequest'
  }

  return Api.post(url, payload, headers);

}

export default {

  login

}
