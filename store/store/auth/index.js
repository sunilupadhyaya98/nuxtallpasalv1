import authService from '@/services/store/auth'

const authModule = {

  namespaced: true,

  state: {

    user_credential: null,

  },

  getters: {

    // GET_IS_BILLABLE: state => {
    //   return state.is_billable;
    // },

  },

  mutations: {

    LOGIN_USER(state, payload) {

      console.log(payload, 'payload');

      state.user_credential = payload;

    }

  },

  actions: {

    login({commit}, payload) {

      return new Promise((resolve, reject) => {

        authService.login(payload).then((response) => {

          commit("LOGIN_USER", response.data.data);

          resolve(response.data);

        }).catch(error => {

          reject(error);

        })

      });

    },

  },

}

export default authModule;
