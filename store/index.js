import Vue from 'vue';
import Vuex from "vuex";
import guestVuex from "@/store/guest";
import storeVuex from "@/store/store";

Vue.use(Vuex);

const allVuexStores = Object.assign({},
  guestVuex.guestVuexState,
  storeVuex.storeVuexState
);


const createStore = () => {
  return new Vuex.Store({
    modules: allVuexStores
  })
}

export default createStore




