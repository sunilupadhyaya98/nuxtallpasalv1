import axios from 'axios';

class Api {

  prepareUrl(endpoint) {
    return process.env.BASE_URL + endpoint;
  }

  getResponseWithParams(url, query, headers) {
    url = this.prepareUrl(url);

    const options = {
      headers: headers,
      params: query
    };

    return axios.get(url, options);
  }

  get(url, headers = {}) {
    url = this.prepareUrl(url);

    const options = {
      headers: headers
    }
    return axios.get(url, options);
  }

  post(url, userData, headers = {}) {
    url = this.prepareUrl(url);

    const options = {
      headers: headers
    };
    return axios.post(url, userData, options);
  }

  put(url, userData, headers = {}) {
    url = this.prepareUrl(url);

    const options = {
      method: 'PUT',
      headers: headers
    };
    return axios.put(url, userData, options);
  }

  delete(url, params, headers = {}) {
    url = this.prepareUrl(url);

    let config = {
      headers: headers
    };

    if (Object.entries(params).length !== 0) {
      config.data = params
    }

    return axios.delete(url, config);
  }

  getResponseType(url, headers = {}, responseType) {
    url = this.prepareUrl(url);

    const options = {
      headers: headers,
      responseType: responseType
    }

    return axios.get(url, options);
  }
}

export default new Api();
